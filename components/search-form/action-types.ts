export const LOAD_LOCAL_STORAGE = 'LOAD_LOCAL_STORAGE';
export const FORM_FIELD_CHANGED = 'FORM_FIELD_CHANGED';
export const SOURCE_SELECTED = 'SOURCE_SELECTED';
export const SOURCE_UNSELECTED = 'SOURCE_UNSELECTED';
