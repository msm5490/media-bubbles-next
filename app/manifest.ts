import type { MetadataRoute } from 'next';

const manifest = (): MetadataRoute.Manifest => ({
	theme_color: '#222',
	background_color: '#a800e6',
	display: 'standalone',
	scope: '/',
	start_url: '/',
	name: 'Media Bubbles',
	short_name: 'Media Bubbles',
	description:
		'Escape your information bubble and view headlines from sources across the political spectrum.',
	icons: [
		{
			src: '/pwa/icon-192x192.png',
			sizes: '192x192',
			type: 'image/png',
		},
		{
			src: '/pwa/icon-256x256.png',
			sizes: '256x256',
			type: 'image/png',
		},
		{
			src: '/pwa/icon-384x384.png',
			sizes: '384x384',
			type: 'image/png',
		},
		{
			src: '/pwa/maskable-icon-512x512.png',
			sizes: '512x512',
			type: 'image/png',
			purpose: 'maskable',
		},
		{
			src: '/pwa/icon-512x512.png',
			sizes: '512x512',
			type: 'image/png',
		},
	],
});

export default manifest;
