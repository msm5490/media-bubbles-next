import type { AlertColor } from '@mui/material';

const ALERT_LEVEL: Record<string, AlertColor> = {
	success: 'success',
	info: 'info',
	warning: 'warning',
	error: 'error',
};

export default ALERT_LEVEL;
